package com.qa.sell;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.Test;

public class sellscript {


	private WebDriver driver;
	private WebElement we;
	private String screenshotName;

	@Test
	public void LoginforUser() throws Exception {
		String driverPath = System.getProperty("user.dir") + "./src/main/resources/com/qa/driver/chromedriver.exe";
//		System.out.println(driverPath);
		System.setProperty("webdriver.chrome.driver","./src/main/resources/com/qa/driver/chromedriver.exe" );
		ChromeOptions options = new ChromeOptions();
		driver = new ChromeDriver(options);
		driver.manage().window().maximize();
		driver.get("http://test-dev.ap-southeast-2.elasticbeanstalk.com/login");
		driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
		
		we = driver.findElement(By.xpath("//input[@id='Email']"));
		we.sendKeys("bdre@wootap.me");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//input[@id='password']"));
		we.sendKeys("Abhi@104007");
		Thread.sleep(1000);
		we = driver.findElement(By.xpath("//button[@id='logbtn']"));
		we.click();	
		Thread.sleep(1000);
		
		JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1250)", "");
        Thread.sleep(1000);
        
        
        we = driver.findElement(By.cssSelector("#investments div:nth-child(3) div[class='show_more_btn']"));
		we.click();	
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//button[contains(text(),'Sell Funds')]"));
		we.click();	
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//input[@class='removedata']"));
		we.sendKeys("1000");	
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[@class='btn_payment1  next1 payment-hover']"));
		we.click();	
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//div[@class='body-box']"));
		we.click();	
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//span[@class='checkmark']"));
		we.click();	
		Thread.sleep(1000);
		
		we = driver.findElement(By.xpath("//a[@class='btn_payment1  next3 payment-hover']"));
		we.click();	
		Thread.sleep(1000);
	
		we = driver.findElement(By.xpath("//a[@class='btn_payment1 next4 last-btn payment-hover']"));
		we.click();	
		Thread.sleep(1000);
	
		we = driver.findElement(By.xpath("//button[@class='confirm']"));
		we.click();	
		Thread.sleep(1000);
		
		driver.quit();
	
	}
}
